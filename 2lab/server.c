#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h> 
#include <stdlib.h>
#include <pthread.h>
#include <curses.h>
#include <signal.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/wait.h>

#define BUF_SIZE		1024
#define MAX_THREAD_NUM 5

typedef struct myArgs_tag {
    int client;
} myArgs_t;

void ZombieHandler(int s) {
    int sys;
    while(wait3(&sys, WNOHANG, (struct rusage*)0) >= 0);
}

void* helloWorld(void *args) {
	char buf[BUF_SIZE];
    int read = 1;
    myArgs_t *arg = (myArgs_t*) args;
    while(read > 0) {
        read = recv(arg->client, buf, BUF_SIZE, 0);
        printf("%s\n", buf);
    }
    return 0;
}

int main(int argc, char *argv[]) {
	int sd;
    int clients[MAX_THREAD_NUM];
    int port;
	struct sockaddr_in saddr;
    struct sockaddr_in caddr;
    socklen_t caddr_len = sizeof(caddr);
    socklen_t saddr_len = sizeof(saddr);
    pthread_t thread[MAX_THREAD_NUM];
    myArgs_t args[MAX_THREAD_NUM];
    int status;
    int status_addr;
    signal(SIGCHLD, ZombieHandler);

    sd = socket(AF_INET, SOCK_STREAM, 0);
    saddr.sin_family = AF_INET;
	saddr.sin_port = htons(0);
	saddr.sin_addr.s_addr = htonl(INADDR_ANY);
    bind(sd, (struct sockaddr *)&saddr, sizeof(saddr));
    
    getsockname(sd, (struct sockaddr *)&saddr, &saddr_len);
    printf("port = %d\n", ntohs(saddr.sin_port));
    
    listen(sd, 1);

    for(int i = 0; i < MAX_THREAD_NUM; i++) {
        clients[i] = accept(sd, (struct sockaddr *)&caddr, &caddr_len);
        args[i].client = clients[i];
        status = pthread_create(&thread[i], NULL, helloWorld, (void*) &args[i]);
        if (status != 0) {
            printf("can't create thread, status = %d\n", status);
            exit(-1);
        }
    }
    for(int i = 0; i < MAX_THREAD_NUM; i++) {
        status = pthread_join(thread[i], (void**)&status_addr);
        if (status != 0) {
            printf("can't join thread, status = %d\n", status);
            exit(-1);
        }
        close(clients[i]);
    }
    close(sd);

    return 0;
}
