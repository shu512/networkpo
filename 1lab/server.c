#include <signal.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h> 
#include <stdlib.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/wait.h>

#define BUF_SIZE		1024

void ZombieHandler(int s) {
    int sys;
    while(wait3(&sys, WNOHANG, (struct rusage*)0) >= 0);
}

int main(int argc, char *argv[]) {
	int sd;
    int client;
	char buf[BUF_SIZE];
    int read = 1;
    int port;
	struct sockaddr_in saddr;
    struct sockaddr_in caddr;
    socklen_t caddr_len = sizeof(caddr);
    socklen_t saddr_len = sizeof(saddr);
    signal(SIGCHLD, ZombieHandler);

    sd = socket(AF_INET, SOCK_STREAM, 0);
    saddr.sin_family = AF_INET;
	saddr.sin_port = htons(0);
	saddr.sin_addr.s_addr = htonl(INADDR_ANY);
    bind(sd, (struct sockaddr *)&saddr, sizeof(saddr));

    getsockname(sd, (struct sockaddr *)&saddr, &saddr_len);
    printf("port = %d\n", ntohs(saddr.sin_port));

    listen(sd, 1);

    while(1) {
        client = accept(sd, (struct sockaddr *)&caddr, &caddr_len);
        int f = fork();
        if (f < 0) {
            printf("fork error\n");
        }
        if (f == 0) {
            close(sd);
            while(read > 0) {
                read = recv(client, buf, BUF_SIZE, 0);
                printf("%s\n", buf);
            }
            close(client);
            exit(0);
        }
    }
    close(sd);
    return 0;
}
