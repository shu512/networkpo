#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h> 
#include <stdlib.h>

#define ADDR_DEFAULT	"127.0.0.1"
#define PORT_DEFAULT	8888

int main(int argc, char *argv[]) {
    char *ip = ADDR_DEFAULT;
    int port = PORT_DEFAULT;
	int sd;
    int server;
    char *sendValue = "1";
	struct sockaddr_in saddr;
    socklen_t saddr_len = sizeof(saddr);

    if ( argc > 1 )
		ip = argv[1];
	if ( argc > 2 )
		port = atoi(argv[2]);
	if ( port < 1024 )
		port = PORT_DEFAULT;
    if ( argc > 3 )
        sendValue = argv[3];

    sd = socket(AF_INET, SOCK_STREAM, 0);
    saddr.sin_family = AF_INET;
	saddr.sin_port = htons(port);
	saddr.sin_addr.s_addr = htonl(INADDR_ANY);

    connect(sd, (struct sockaddr *) &saddr, sizeof(saddr));
    int pauseSec = atoi(sendValue);
    for(int i = 0; i < 100; i++) {
        sleep(pauseSec);
        send(sd, sendValue, sizeof(sendValue), 0);
    }

    close(sd);

    return 0;
}