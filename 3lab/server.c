#include <signal.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h> 
#include <stdlib.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/wait.h>
#include <unistd.h>
#include <fcntl.h>

#define BUF_SIZE		1024

int main(int argc, char *argv[]) {
	int sd;
    int client[10];
	char buf[BUF_SIZE];
    int read = 1;
    int port;
	struct sockaddr_in saddr;
    struct sockaddr_in caddr;
    socklen_t caddr_len = sizeof(caddr);
    socklen_t saddr_len = sizeof(saddr);
    int numb_client = 0;
    int fdmax;
    int newfd;
    fd_set readfds;
    fd_set master;

    FD_ZERO(&readfds);
    FD_ZERO(&master);
    sd = socket(AF_INET, SOCK_STREAM, 0);
    fdmax = sd;
    saddr.sin_family = AF_INET;
	saddr.sin_port = htons(0);
	saddr.sin_addr.s_addr = htonl(INADDR_ANY);
    bind(sd, (struct sockaddr *)&saddr, sizeof(saddr));    
    getsockname(sd, (struct sockaddr *)&saddr, &saddr_len);
    printf("port = %d\n", ntohs(saddr.sin_port));

    fcntl(sd, F_SETFL, O_NONBLOCK);
    FD_SET(sd, &master);
    listen(sd, 1);
    while(1) {
        readfds = master; // копируем в дополнительный сет(т.к. select его затрёт)
        if (select(fdmax+1, &readfds, NULL, NULL, NULL) == -1) { 
            perror("select"); 
            exit(-1); 
        }
        for (int i = 0; i <= fdmax; i++) {
            if (FD_ISSET(i, &readfds)) { // нам что-то пришло
                if (i == sd) { // обработка новых соединений 
                    client[numb_client] = accept(sd, (struct sockaddr *)&caddr, &caddr_len);
                    fcntl(client[numb_client], F_SETFL, O_NONBLOCK);
                    FD_SET(client[numb_client], &master);
                    newfd = client[numb_client];
                    if (newfd > fdmax) { // отслеживаем максимальный дескриптор для корректной работы select
                        fdmax = newfd;
                    }
                    numb_client++;
                } else { // пришло сообщение от клиента
                    read = recv(i, buf, BUF_SIZE, 0);
                    if (read == 0) { // клиент отсоединился
                        FD_CLR(i, &master);
                        close(i);
                        printf("client out\n");    
                    } else {
                        printf("%s\n", buf);
                    }
                }
            }
        }
        
    }
    close(sd);
    return 0;
}
